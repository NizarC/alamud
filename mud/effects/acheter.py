# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect3
from mud.events import AcheterEvent

class AcheterEffect(Effect3):
    EVENT = AcheterEvent
