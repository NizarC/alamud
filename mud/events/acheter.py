# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class AcheterEvent(Event3):
    NAME = "acheter"

    def perform(self):
        if not self.object.has_prop("achetable"):
            self.add_prop("object-not-takable")
            return self.acheter_failed()
        if not self.object2.has_prop("argent"):
            return self.acheter_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.acheter_failed()
        self.object.move_to(self.actor)
        self.inform("acheter")

    def acheter_failed(self):
        self.fail()
        self.inform("acheter.failed")
